<?php


namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;


class JWTCreatedListener
{
    /**
     * @param JWTCreatedEvent $event
     *
     * @return void
     * Event listener for when a JWT token is created, override expiration date
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {

        $expiration = new \DateTime('+1 day');
        $expiration->setTime(2, 0, 0);

        $payload        = $event->getData();
        $payload['exp'] = $expiration->getTimestamp();
        $user = $event->getUser();
        $payload['user'] = $user;

        $event->setData($payload);
    }
}