<?php


namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\Security\Core\User\UserInterface;


class AuthenticationSuccessListener
{
    /**
     * @param AuthenticationSuccessEvent $event
     * Event listener for when a user successfully login, return user's informations with JWT token
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        $user = $event->getUser();

        $data['data'] = array(
            'id' => $user->getId(),
            'role' => $user->getClientRole(),
            'email' => $user->getUsername(),
            'name' => $user->getName(),
            'address' => $user->getAddress(),
            'city' => $user->getCity()
        );

        $event->setData($data);
    }
}