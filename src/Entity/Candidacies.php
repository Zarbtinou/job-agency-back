<?php

namespace App\Entity;

use App\Repository\CandidaciesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CandidaciesRepository::class)
 */
class Candidacies
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="candidacies")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=JobOffers::class, inversedBy="candidacies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $job_offer;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $files = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getJobOffer(): ?JobOffers
    {
        return $this->job_offer;
    }

    public function setJobOffer(?JobOffers $job_offer): self
    {
        $this->job_offer = $job_offer;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getFiles(): ?array
    {
        return $this->files;
    }

    public function setFiles(?array $files): self
    {
        $this->files = $files;

        return $this;
    }
}
