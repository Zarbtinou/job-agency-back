<?php

namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
/**
 * @ORM\Table(name="user")
 * @ORM\Entity
 */
class User implements UserInterface
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $client_role;

    /**
     * @ORM\OneToMany(targetEntity=JobOffers::class, mappedBy="owner_id")
     */
    private $own_offers;

    /**
     * @ORM\OneToMany(targetEntity=Candidacies::class, mappedBy="user")
     */
    private $candidacies;

    /**
     * @ORM\ManyToMany(targetEntity=JobOffers::class)
     */
    private $favorite_offers;

    public function __construct()
    {
        $this->own_offers = new ArrayCollection();
        $this->candidacies = new ArrayCollection();
        $this->favorite_offers = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @return string|null
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @return string|null
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }
    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return array|string[]
     */
    public function getRoles()
    {
        return array('ROLE_CANDIDATE', 'ROLE_RECRUITER');
    }

    public function eraseCredentials()
    {
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClientRole(): ?string
    {
        return $this->client_role;
    }

    public function setClientRole(string $client_role): self
    {
        $this->client_role = $client_role;

        return $this;
    }

    /**
     * @return Collection|JobOffers[]
     */
    public function getOwnOffers(): Collection
    {
        return $this->own_offers;
    }

    public function addOwnOffer(JobOffers $ownOffer): self
    {
        if (!$this->own_offers->contains($ownOffer)) {
            $this->own_offers[] = $ownOffer;
            $ownOffer->setOwnerId($this);
        }

        return $this;
    }

    public function removeOwnOffer(JobOffers $ownOffer): self
    {
        if ($this->own_offers->removeElement($ownOffer)) {
            // set the owning side to null (unless already changed)
            if ($ownOffer->getOwnerId() === $this) {
                $ownOffer->setOwnerId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Candidacies[]
     */
    public function getCandidacies(): Collection
    {
        return $this->candidacies;
    }

    public function addCandidacy(Candidacies $candidacy): self
    {
        if (!$this->candidacies->contains($candidacy)) {
            $this->candidacies[] = $candidacy;
            $candidacy->setUser($this);
        }

        return $this;
    }

    public function removeCandidacy(Candidacies $candidacy): self
    {
        if ($this->candidacies->removeElement($candidacy)) {
            // set the owning side to null (unless already changed)
            if ($candidacy->getUser() === $this) {
                $candidacy->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|JobOffers[]
     */
    public function getFavoriteOffers(): Collection
    {
        return $this->favorite_offers;
    }

    public function addFavoriteOffer(JobOffers $favoriteOffer): self
    {
        if (!$this->favorite_offers->contains($favoriteOffer)) {
            $this->favorite_offers[] = $favoriteOffer;
        }

        return $this;
    }

    public function removeFavoriteOffer(JobOffers $favoriteOffer): self
    {
        $this->favorite_offers->removeElement($favoriteOffer);

        return $this;
    }
}