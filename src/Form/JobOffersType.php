<?php

namespace App\Form;

use App\Entity\JobOffers;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JobOffersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('description', TextType::class)
            ->add('city', TextType::class)
            ->add('salary', IntegerType::class)
            ->add('duration', IntegerType::class)
            ->add('contract_type', TextType::class)
            ->add('company', TextType::class)
            ->add('expiration_date', DateType::class)
            ->add('owner_id', EntityType::class, [
                "class" => User::class
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => JobOffers::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }
}
