<?php

namespace App\Controller;


use App\Entity\JobOffers;
use App\Entity\User;
use App\Form\JobOffersType;
use App\Repository\JobOffersRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;

/**
 * Class JobOffersController
 * @package App\Controller
 * @Route("/job_offers", name="job_offers_api")
 *
 * CRUD methods for JobOffers Object
 */
class JobOffersController extends AbstractController
{
    private $apiController;
    public function __construct(ApiController $apiController)
    {
        $this->apiController = $apiController;
    }

    /**
     * @param Request $request
     * @param JobOffersRepository $jobOffersRepository
     * @return JsonResponse
     * @Route("/search", name="last_job_offers", methods={"GET"})
     * Return all job offers
     */
    public function getJobOffers(Request $request,JobOffersRepository $jobOffersRepository): JsonResponse
    {

        $sql = "SELECT * FROM job_offers WHERE 1 ";
        if ($request->get('title')) {
            $sql .= " AND title LIKE '%". $request->get('title') ."%'OR description LIKE '%". $request->get('title') ."%'";
        }
        if ($request->get('city')) {
            $sql .= "AND city LIKE '%". $request->get('city') ."%'";
        }
        if ($request->get('owner', null)) {
            $sql .= " AND owner_id_id = " . $request->get('owner');
        }
        $sql .= " ORDER BY create_date DESC";
        if ($request->get('limit')) {
            $sql .= " LIMIT " . $request->get('limit');
        }
        $conn = $this->getDoctrine()->getManager()->getConnection();

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $data = [];
        foreach($stmt->fetchAll() as $row) {
            $data[] = $row;
        }
        return $this->apiController->response($data);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param JobOffersRepository $jobOffersRepository
     * @return JsonResponse
     * @Route("/new", name="job_offers_new", methods={"POST"})
     * Create a new job offer
     * @throws Exception
     */
    public function addJobOffers( Request $request, EntityManagerInterface $entityManager, JobOffersRepository $jobOffersRepository): JsonResponse
    {
        $request->request->set('title', $request->get('name'));
        $request->request->remove('name');
        $request = $this->apiController->transformJsonBody($request);

        //$request = json_decode($request->getContent(), true);

        $job_offers = new JobOffers();
        $form = $this->createForm(JobOffersType::class, $job_offers);
        $form->submit(json_decode($request->getContent(), true));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($job_offers);
            $entityManager->flush();

            $data = [
                'status' => 200,
                'success' => "JobOffer added successfully",
            ];

        } else {
            $data = [
                'status' => 500,
                'error' => "An error occured",
                'errors' => $form->getErrors()->__toString(),
                'submited' => $form->isSubmitted(),
                'validation' => $form->isValid(),
                'data' => json_decode($request->getContent(), true)

            ];
        }

        return $this->apiController->response($data);

        /**$jobOffers = new JobOffers();
        $jobOffers->setName($request->get('name'));
        $jobOffers->setDescription($request->get('description'));
        $jobOffers->setCity($request->get('city'));
        $jobOffers->setSalary($request->get('salary'));
        $jobOffers->setDuration($request->get('duration'));
        $jobOffers->setContractType($request->get('contract_type'));
        $jobOffers->setCompany($request->get('company'));
        $jobOffers->setExpirationDate(new \DateTime($request->get('expiration_date')));
        $jobOffers->setOwnerId($owner);
        $entityManager->persist($jobOffers);
        $entityManager->flush()
         * */;

        $data = [
            'status' => 200,
            'success' => "JobOffer added successfully",
        ];
        return $this->apiController->response($data);

    }


    /**
     * @param JobOffersRepository $jobOffersRepository
     * @param $id
     * @return JsonResponse
     * @Route("/{id}", name="job_offer_get", methods={"GET"})
     * Return specific job offer with his id
     */
    public function getJobOffer(JobOffersRepository $jobOffersRepository, $id){
        $jobOffers = $jobOffersRepository->find($id);

        if (!$jobOffers){
            $data = [
                'status' => 404,
                'errors' => "JobOffer not found",
            ];
            return $this->apiController->response($data, 404);
        }
        return $this->apiController->response($jobOffers);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param JobOfferRepository $jobOffersRepository
     * @param $id
     * @return JsonResponse
     * @Route("/job_offers/{id}", name="job_offers_put", methods={"PUT"})
     * Update a job offer with his id
     */
    public function updateJobOffer(Request $request, EntityManagerInterface $entityManager, JobOfferRepository $jobOffersRepository, $id){

        try{
            $jobOffers = $jobOffersRepository->find($id);

            if (!$jobOffers){
                $data = [
                    'status' => 404,
                    'errors' => "JobOffer not found",
                ];
                return $this->apiController->response($data, 404);
            }

            $request = $this->apiController->transformJsonBody($request);

            if (!$request || !$request->get('name') || !$request->request->get('description')){
                throw new Exception();
            }

            $jobOffers->setName($request->get('name'));
            $jobOffers->setDescription($request->get('description'));
            $entityManager->flush();

            $data = [
                'status' => 200,
                'errors' => "JobOffer updated successfully",
            ];
            return $this->apiController->response($data);

        }catch (Exception $e){
            $data = [
                'status' => 422,
                'errors' => "Data no valid",
            ];
            return $this->apiController->response($data, 422);
        }

    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param UserRepository $userRepository
     * @return JsonResponse
     * @Route("/add-favori", name="job_offers_add", methods={"POST"})
     * Add job offer to user favorites
     */
    public function addInFavorites( Request $request, EntityManagerInterface $entityManager, UserRepository $userRepository){

        $request = $this->apiController->transformJsonBody($request);

        $user = $entityManager->getRepository(User::class)->find($request->get('user_id'));
        $job_offer = $entityManager->getRepository(JobOffers::class)->find($request->get('joboffer_id'));

        $user->addFavoriteOffer($job_offer);
        $entityManager->persist($user);
        $entityManager->flush();

        $data = [
            'status' => 200,
            'success' => "Offer added to favorites",
        ];
        return $this->apiController->response($data);

    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param UserRepository $userRepository
     * @return JsonResponse
     * @Route("/favori", name="user_favorite", methods={"POST"})
     * List of favorite offers
     */
    public function userFavorites( Request $request, EntityManagerInterface $entityManager, UserRepository $userRepository){

        $request = $this->apiController->transformJsonBody($request);

        $user = $entityManager->getRepository(User::class)->find($request->get('user_id'));
        $array = [];

        foreach ($user->getFavoriteOffers() as $favorite) {
            $array[] = $favorite;
        }

        $data = [
            'status' => 200,
            'success' => "List favori of the user",
            'job_offers' => $array
        ];
        return $this->apiController->response($data);

    }

    /**
     * @param JobOfferRepository $jobOffersRepository
     * @param $id
     * @return JsonResponse
     * @Route("/job_offers/{id}", name="job_offers_delete", methods={"DELETE"})
     * Delete a job offer with his id
     */
    public function deleteJobOffer(EntityManagerInterface $entityManager, JobOfferRepository $jobOffersRepository, $id){
        $jobOffers = $jobOffersRepository->find($id);

        if (!$jobOffers){
            $data = [
                'status' => 404,
                'errors' => "JobOffer not found",
            ];
            return $this->apiController->response($data, 404);
        }

        $entityManager->remove($jobOffers);
        $entityManager->flush();
        $data = [
            'status' => 200,
            'errors' => "JobOffer deleted successfully",
        ];
        return $this->apiController->response($data);
    }
}