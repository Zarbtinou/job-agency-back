<?php

namespace App\Controller;

use App\Entity\Candidacies;
use App\Entity\JobOffers;
use App\Entity\User;
use App\Repository\JobOffersRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CandidaciesRepository;

/**
 * Class JobOffersController
 * @package App\Controller
 * @Route("/candidacies", name="candidacies_api")
 *
 * CRUD methods for JobOffers Object
 */
class CandidaciesController extends AbstractController
{
    private $apiController;
    public function __construct(ApiController $apiController)
    {
        $this->apiController = $apiController;
    }

    /**
     * @param Request $request
     * @param CandidaciesRepository $candidaciesRepository
     * @return JsonResponse
     * @Route( name="candidacies", methods={"GET"})
     */
    public function getUserCandidacies(Request $request, candidaciesRepository $candidaciesRepository): JsonResponse{
        $sql = "SELECT * FROM candidacies LEFT JOIN job_offers ON candidacies.job_offer_id = job_offers.id WHERE candidacies.user_id=";
        $sql .= $request->get('user_id');
        $conn = $this->getDoctrine()->getManager()->getConnection();

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $data = [];
        foreach($stmt->fetchAll() as $row) {
            $data[] = $row;
        }
        return $this->apiController->response($data);
    }

    /**
     * @param Request $request
     * @param CandidaciesRepository $candidaciesRepository
     * @param $id
     * @return JsonResponse
     * @Route("/joboffer-candidacies/{id}") name="candidacies", methods={"GET"})
     */
    public function getJobOfferCandidacies(Request $request, candidaciesRepository $candidaciesRepository, $id): JsonResponse{
        $sql = "SELECT * FROM candidacies LEFT JOIN user ON candidacies.user_id = user.id WHERE candidacies.job_offer_id=" . $id;
        $conn = $this->getDoctrine()->getManager()->getConnection();

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $data = [];
        foreach($stmt->fetchAll() as $row) {
            $data[] = $row;
        }
        return $this->apiController->response($data);
    }

    /**
     * @param Request $request
     * @param CandidaciesRepository $candidaciesRepository
     * @return JsonResponse
     * @Route("/accept") name="accept", methods={"GET"})
     */
    public function acceptCandidacy(Request $request, candidaciesRepository $candidaciesRepository ): JsonResponse{
        $sql = "UPDATE candidacies SET status = ";
        $user_id = $request->get('user_id');
        $job_offer_id = $request->get('job_offer_id');
        if ($request->get('action') === "accept") {
            $sql .= '"accepted"';
        } else if ($request->get('action') === "decline") {
            $sql .= '"refused"';
        }
        $sql .= " WHERE user_id = $user_id AND job_offer_id = $job_offer_id";
        $conn = $this->getDoctrine()->getManager()->getConnection();

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $data = [
            'status' => 200,
            'success' => "Candidacy activated",
        ];

        return $this->apiController->response($data);
    }


    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param CandidaciesRepository $candidaciesRepository
     * @return JsonResponse
     * @throws Exception
     * @Route("/add", name="candidacy_add", methods={"POST"})
     * Create a new candidacy
     */
    public function addCandidacy( Request $request, EntityManagerInterface $entityManager, CandidaciesRepository $candidaciesRepository){
        $request = $this->apiController->transformJsonBody($request);
        $user = $entityManager->getRepository(User::class)->find($request->get('user_id'));
        $jobOffer = $entityManager->getRepository(JobOffers::class)->find($request->get('joboffer_id'));
// Count total files
        $countfiles = count($_FILES['file']['name']);
        $filePath = array();
        if (file_exists("users-files/".$user->getId()) === false) {
            mkdir("users-files/".$user->getId(), "0777", true);
        }
        // Looping all files
        for($i=0;$i<$countfiles;$i++){
            if ($i > 3) {
                break;
            }
            $filename = $_FILES['file']['name'][$i];
            // Upload file
            move_uploaded_file($_FILES['file']['tmp_name'][$i],'users-files/'.$user->getId()."/".$filename);
            array_push($filePath, $filename);
        }

        $candidacy = new Candidacies();
        $candidacy->setStatus("pending");
        $candidacy->setJobOffer($jobOffer);
        $candidacy->setUser($user);
        $candidacy->setFiles($filePath);
        $entityManager->persist($candidacy);
        $entityManager->flush();

        $data = [
            'status' => 200,
            'success' => "Candidacy added successfully",
        ];
        return $this->apiController->response($data);

    }
}
