<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210116164105 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE candidacies (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, job_offer_id INT NOT NULL, status VARCHAR(255) NOT NULL, files LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_CA9ED74DA76ED395 (user_id), INDEX IDX_CA9ED74D3481D195 (job_offer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE candidacies ADD CONSTRAINT FK_CA9ED74DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE candidacies ADD CONSTRAINT FK_CA9ED74D3481D195 FOREIGN KEY (job_offer_id) REFERENCES job_offers (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE candidacies');
    }
}
