<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210118020352 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_job_offers (user_id INT NOT NULL, job_offers_id INT NOT NULL, INDEX IDX_2D25EF4AA76ED395 (user_id), INDEX IDX_2D25EF4A67205B3F (job_offers_id), PRIMARY KEY(user_id, job_offers_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_job_offers ADD CONSTRAINT FK_2D25EF4AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_job_offers ADD CONSTRAINT FK_2D25EF4A67205B3F FOREIGN KEY (job_offers_id) REFERENCES job_offers (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user_job_offers');
    }
}
