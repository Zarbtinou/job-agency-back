<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210116124826 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE job_offers ADD owner_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE job_offers ADD CONSTRAINT FK_8A4229A68FDDAB70 FOREIGN KEY (owner_id_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_8A4229A68FDDAB70 ON job_offers (owner_id_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE job_offers DROP FOREIGN KEY FK_8A4229A68FDDAB70');
        $this->addSql('DROP INDEX IDX_8A4229A68FDDAB70 ON job_offers');
        $this->addSql('ALTER TABLE job_offers DROP owner_id_id');
    }
}
